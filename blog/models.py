from django.db import models


# Tags model
class Tag(models.Model):
    name = models.CharField(verbose_name="Tag name", max_length=64)

    def __str__(self):
        return f"{self.name}"


# Category model
class Category(models.Model):
    name = models.CharField(verbose_name="Category name", max_length=64)

    def __str__(self):
        return f"{self.name}"


# Rating model
class Rating(models.Model):
    post = models.ForeignKey(
        "Post",
        on_delete=models.CASCADE,
        verbose_name="Post",
        blank=False,
        null=False
    )
    value = models.PositiveIntegerField(default=1)
    user = models.ForeignKey(
        "auth.User",
        on_delete=models.CASCADE,
        verbose_name="Author",
        blank=True,
        null=True
    )

    def __str__(self):
        return f"{self.post.title} - {self.user} -{self.value}"


# Post model
class Post(models.Model):
    author = models.ForeignKey(
        "auth.User",
        on_delete=models.CASCADE,
        verbose_name="Author",
        related_name="posts",
        blank=True,
        null=True
    )
    category = models.ForeignKey(
        Category,
        on_delete=models.CASCADE,
        related_name="posts",
        blank=True,
        null=True
    )
    title = models.CharField(max_length=64)
    text = models.TextField()
    short_text = models.TextField(max_length=256, blank=False, null=False)
    publish_date = models.DateTimeField(auto_now_add=True)
    quantity_views = models.PositiveIntegerField(default=0)
    post_rating = models.FloatField(default=1)
    vote_users = models.ManyToManyField("auth.User", blank=True, null=True)
    tags = models.ManyToManyField(Tag, blank=True, null=True)
    is_published = models.BooleanField(blank=False, null=False, default=True)

    def __str__(self):
        return f"{self.title}"

    # Счетчик количества коментариев
    @property
    def comment_count(self):
        return self.comment_set.all().count()

    # @property
    # def tags_viewer(self):
    #     tags = self.tags.all()
    #     a = []
    #     for tag in tags:
    #         a.append(str(tag))
    #     return a


# Comment model
class Comment(models.Model):
    author = models.ForeignKey(
        "auth.User",
        on_delete=models.CASCADE,
        verbose_name="Author",
        blank=False,
        null=False
    )
    post = models.ForeignKey(
        "Post",
        on_delete=models.CASCADE,
        verbose_name="Post",
        blank=False,
        null=False
    )
    text = models.TextField()
    publish_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.post.pk} - {self.publish_date}"
