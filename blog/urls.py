from django.urls import path
from blog.views import post_list, post_detail, post_new, post_edit, post_delete, new_comment, \
    delete_comment, edit_comment, new_rate, category_post_list, categories, tags_post_list, \
    tags, user_detail, search, user_post_list, user_draft_list, comment_list

urlpatterns = [
    path("", post_list, name="post_list"),
    path("detail/<int:post_pk>", post_detail, name="post_detail"),
    path("post_new/", post_new, name="post_new"),
    path("post_edit/<int:post_pk>", post_edit, name="post_edit"),
    path("post_delete/<int:post_pk>", post_delete, name="post_delete"),
    path("new_comment/<int:post_pk>", new_comment, name="new_comment"),
    path("delete_comment/<int:post_pk>/<int:comment_pk>", delete_comment, name="delete_comment"),
    path("edit_comment/<int:post_pk>/<int:comment_pk>", edit_comment, name="edit_comment"),
    path("new_rate/<int:post_pk>/<int:variable>", new_rate, name="new_rate"),
    path("category_post_list/<int:category_pk>", category_post_list, name="category_post_list"),
    path("tags_post_list/<int:tag_pk>", tags_post_list, name="tags_post_list"),
    path("categories", categories, name="categories"),
    path("tags", tags, name="tags"),
    path("user_detail/<int:user_pk>", user_detail, name="user_detail"),
    path("user_post_list/<int:user_pk>", user_post_list, name="user_post_list"),
    path("user_draft_list/<int:user_pk>", user_draft_list, name="user_draft_list"),
    path("search", search, name="search"),
    path("comment_list/<int:user_pk>", comment_list, name="comment_list")
]
