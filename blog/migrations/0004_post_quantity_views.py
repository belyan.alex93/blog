# Generated by Django 3.2 on 2021-05-04 18:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0003_rename_comments_comment'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='quantity_views',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
