from django.contrib import admin
from blog.models import Post, Comment, Category, Rating, Tag

admin.site.register(Post)
admin.site.register(Comment)
admin.site.register(Category)
admin.site.register(Rating)
admin.site.register(Tag)
