from django import forms
from blog.models import Post, Comment


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = [
            "title",
            "category",
            "short_text",
            "text",
            "tags",
            "is_published",
        ]


class CommentsForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = [
            "text"
        ]



