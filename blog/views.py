from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from django.contrib.auth.models import User
from blog.forms import PostForm, CommentsForm
from blog.models import Post, Comment, Rating, Category, Tag
from blog.utils import rate_counter, message_creator


def post_list(request):
    posts = Post.objects.filter(is_published=True)
    drafts = Post.objects.filter(is_published=False)
    for post in posts:
        rate_counter(post)
    return render(request, "blog/post_list.html", {"posts": posts, "drafts": drafts, "title": "Main page"})


def post_detail(request, post_pk):
    post = get_object_or_404(Post, pk=post_pk)
    rate_counter(post)
    comments = Comment.objects.filter(post=post)
    post.quantity_views += 1
    post.save()
    return render(request, "blog/post_detail.html",
                  {"post": post,
                   "comments": comments,
                   "count": comments.count(),
                   "title": f"{post.title}"
                   })


def post_new(request):
    form = PostForm
    if request.method == "GET":
        return render(request, "blog/post_form.html", {"form": form, "title": "New post"})
    elif request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.publish_date = timezone.now()
            post.save()
            post.tags.add(*[tag for tag in form.cleaned_data['tags']])
            return redirect("post_detail", post_pk=post.pk)


def post_edit(request, post_pk):
    post = get_object_or_404(Post, pk=post_pk)
    if request.method == "GET":
        form = PostForm(instance=post)
        return render(request, "blog/post_form.html", {"form": form, "title": "Edit post"})
    elif request.method == "POST":
        form = PostForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.publish_date = timezone.now()
            post.save()
            post.tags.add(*[tag for tag in form.cleaned_data['tags']])
            return redirect("post_detail", post_pk=post.pk)


def post_delete(request, post_pk):
    get_object_or_404(Post, pk=post_pk).delete()
    return redirect("post_list")


def new_comment(request, post_pk):
    form = CommentsForm
    if request.method == "GET":
        return render(request, "blog/Forms.html", {"form": form, "title": "New comment"})
    elif request.method == "POST":
        form = CommentsForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.post = Post.objects.filter(pk=post_pk).first()
            comment.author = request.user
            comment.publish_date = timezone.now()
            comment.save()
            pk = post_pk
            return redirect("post_detail", post_pk=pk)


def delete_comment(request, comment_pk, post_pk):
    get_object_or_404(Comment, pk=comment_pk).delete()
    return redirect("post_detail", post_pk=post_pk)


def edit_comment(request, comment_pk, post_pk):
    comment = get_object_or_404(Comment, pk=comment_pk)
    if request.method == "GET":
        form = CommentsForm(instance=comment)
        return render(request, "blog/Forms.html", {"form": form, "title": "Edit comment"})
    elif request.method == "POST":
        form = CommentsForm(request.POST, instance=comment)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.post = Post.objects.filter(pk=post_pk).first()
            comment.author = request.user
            comment.publish_date = timezone.now()
            comment.save()
            pk = post_pk
            return redirect("post_detail", post_pk=pk)


def new_rate(request, post_pk, variable):
    post = Post.objects.filter(pk=post_pk).first()
    Rating.objects.create(value=variable, post=post)
    post.vote_users.add(request.user)
    rate_counter(post)
    return redirect("post_detail", post_pk=post_pk)


def category_post_list(request, category_pk):
    category = get_object_or_404(Category, pk=category_pk)
    posts = Post.objects.filter(category=category_pk)
    return render(request, "blog/post_list.html", {"posts": posts,
                                                   "category_name": category,
                                                   "title": "Category post list"})


def categories(request):
    cats = Category.objects.all()
    return render(request, "blog/categories.html", {"categories": cats, "title": "Categories"})


def tags_post_list(request, tag_pk):
    tag = get_object_or_404(Tag, pk=tag_pk)
    posts = tag.post_set.all()
    return render(request, "blog/post_list.html", {"posts": posts,
                                                   "tag_name": tag,
                                                   "title": "Tags post list"})


def tags(request):
    tag_list = Tag.objects.all()
    return render(request, "blog/tags_list.html", {"tags": tag_list, "title": "Tags cloud"})


def user_detail(request, user_pk):
    C_user = get_object_or_404(User, pk=user_pk)
    return render(request, "blog/user_detail.html", {"c_user": C_user, "title": f"{C_user.username} info"})


def search(request):
    if request.method == "GET":
        return render(request, "blog/search.html", {"title": "Search"})
    elif request.method == "POST":
        new_list = []
        search_input = request.POST.get('search')
        posts = Post.objects.filter(is_published=True).all()
        for post in posts:
            if search_input in post.title or \
                    search_input in post.text or \
                    search_input in post.short_text:
                new_list.append(post)
                print(type(new_list))
        message = message_creator(len(new_list))
        return render(request, "blog/search.html", {"new_list": new_list, "message": message, "title": "Search"})


def user_post_list(request, user_pk):
    posts = Post.objects.filter(author=user_pk, is_published=True).all()
    username = User.objects.filter(pk=user_pk).first().username
    return render(request, "blog/post_list.html", {"posts": posts, "title": f"{username} posts list"})


def user_draft_list(request, user_pk):
    posts = Post.objects.filter(author=user_pk, is_published=False).all()
    username = User.objects.filter(pk=user_pk).first().username
    return render(request, "blog/post_list.html", {"posts": posts, "title": f"{username} drafts list"})


def comment_list(request, user_pk):
    comments = Comment.objects.filter(author=user_pk).all()
    username = User.objects.filter(pk=user_pk).first().username
    return render(request, "blog/comment_list.html", {"comments": comments, "title": f"{username} comments list"})
