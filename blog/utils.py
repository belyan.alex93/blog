from blog.models import Rating


def rate_counter(post):
    lst = [int(rate.value) for rate in list(Rating.objects.filter(post=post))]
    try:
        rating = round(sum(lst) / len(lst), 1)
    except ZeroDivisionError:
        rating = 1
    post.post_rating = rating
    post.save()


def message_creator(length):
    message = ""
    if length == 0:
        message = "Ничего не найдено"
    elif ost_ot_del(length) == 1:
        message = f"Найден {length} пост"
    elif ost_ot_del(length) in range(2, 5):
        message = f"Найдено {length} поста"
    elif ost_ot_del(length) in range(5, 16):
        message = f"Найдено {length} поста"
    return message


def ost_ot_del(number):
    a = number
    while a not in range(1, 16):
        a = a % 10
    return a
